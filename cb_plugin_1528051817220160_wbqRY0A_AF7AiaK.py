'''
 Name: Regenerate Options For Environment Autoselect
 Modified:
    2018/05/30 smanross - Initial Version (third revision)

Tested:
    Cloudbolt version: 8.1

Description:
    Generate an option list for parameters:
 Requires:
    *Cloudbolt Parameters:
        sens_q1 (String),
        sens_q2 (String),
        sens_q3 (String),
        sens_q4 (String)
        sensitivity_classification (string)
    *FieldDependency:
        sens_q2 dependent on sens_q1
        sens_q3 dependent on sens_q2
        sens_q4 depdependent on sens_q3
        sensitivity_classification dependent on sens_q4
        sens_places dependent on sens_q4

    #create paramters
    q1, _ = CustomField.objects.get_or_create(name="sens_q1",
        label="What Environment Type should we build in?",
        type='STR')
    q2, _ = CustomField.objects.get_or_create(name="sens_q2",
        label="What is the Data Class for this server?",
        type='STR')
    q3, _ = CustomField.objects.get_or_create(name="sens_q3",
        label="Is this a Production Server?",
        type='STR')
    q4, _ = CustomField.objects.get_or_create(name="sens_q4",
        label="Does this server require external network access?",
        type='STR')
    sens_class, _ = CustomField.objects.get_or_create(name="sensitivity_classification",
        label="Sensitivity Classification = ",
        type='STR')
    avail, _ = CustomField.objects.get_or_create(name="sens_places",
        label="Sensitivity Environments Available = ",
        type='STR')
    #create field dependencies
    q2fd = FieldDependency.objects.create(dependent_field_id=q2.id,
        controlling_field_id=q1.id,
        dependency_type='REGENOPTIONS')
    q3fd = FieldDependency.objects.create(dependent_field_id=q3.id,
        controlling_field_id=q2.id,
        dependency_type='REGENOPTIONS')
    q4fd = FieldDependency.objects.create(dependent_field_id=q4.id,
        controlling_field_id=q3.id,
        dependency_type='REGENOPTIONS')
    sensfd = FieldDependency.objects.create(dependent_field_id=sens_class.id,
        controlling_field_id=q4.id,
        dependency_type='REGENOPTIONS')
    availfd = FieldDependency.objects.create(dependent_field_id=avail.id,
        controlling_field_id=sens_class.id,
        dependency_type='REGENOPTIONS')

Documentation:
    Regenerated Options:
        https://<yourcbserver>/static-a5d6c7b/docs/advanced/orchestration-actions/cloudbolt-plugins/generated_parameter_options.html?highlight=get_options_list
'''

from utilities.logger import ThreadLogger
from accounts.models import Group
from infrastructure.models import Environment


#in the event you arent allowed to build a server due to classification, email
#  this group
RESTRICTED_EMAIL_GROUP = 'mpanek@cloudbolt.io'

LOGGER = ThreadLogger('__name__')

def get_options_list(field=None, **kwargs):
    '''
        creates lists for dependent regenerated option lists
        available to get_options_list: blueprint=None, group=None,
    '''

    values = []

    #LOGGER.info('in regen options for sensitivity: {}'.format(field.name))
    if field.name in  ['sens_q1',
                       'sens_q2',
                       'sens_q3',
                       'sens_q4',
                       'sensitivity_classification',
                       'sens_places']:

        form_data = kwargs.get('form_data')
        form_prefix = kwargs.get('form_prefix')

        if form_prefix:
            #order form phase:  list options to user
            values = get_options_for_field(field=field,
                                           form_data=form_data,
                                           form_prefix=form_prefix)
        else:
            #validation phase: list all possible options so OFV can validate
            #                  any option the user chooses

            values = get_all_options_for_field(field=field)

    return values

def get_options_for_field(field=None, form_data=None, form_prefix=None):
    '''
        get the options for a field to display to the order form
    '''

    if field.name == 'sens_q2':
        #Data Class - what data class is the server in?
        env_type_name = get_value_from_form_data(form_data=form_data,
                                                 form_prefix=form_prefix,
                                                 q_name=field.name,
                                                 value_name='sens_q1')

        LOGGER.info('{} env_type_name = {}'.format(field.name, env_type_name))
        if env_type_name != '':
            values = get_sensitivity_type(q_1=env_type_name)
            #LOGGER.info('{} values = {}'.format(field.name, values))
            return values

    elif field.name == 'sens_q3':
        #Production - Is the Production status == Production? (Yes/No)
        env_type_name = get_value_from_form_data(form_data=form_data,
                                                 form_prefix=form_prefix,
                                                 q_name=field.name,
                                                 value_name='sens_q1')

        data_class = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q2')

        if data_class != '' and env_type_name != '' and form_data:
            values = get_sensitivity_type(q_1=env_type_name,
                                          q_2=data_class)
            #LOGGER.info('{} values = {}'.format(field.name, values))
            return values

    elif field.name == 'sens_q4':
        #External Network - Is the Network External? Yes / No
        env_type_name = get_value_from_form_data(form_data=form_data,
                                                 form_prefix=form_prefix,
                                                 q_name=field.name,
                                                 value_name='sens_q1')

        data_class = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q2')

        production = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q3')

        if data_class != '' and env_type_name != '' and production != '' and form_data:
            values = get_sensitivity_type(q_1=env_type_name,
                                          q_2=data_class,
                                          q_3=production)
            #LOGGER.info('{} values = {}'.format(field.name, values))
            return values

    elif field.name == 'sensitivity_classification':
        #Sensitivity Classification
        env_type_name = get_value_from_form_data(form_data=form_data,
                                                 form_prefix=form_prefix,
                                                 q_name=field.name,
                                                 value_name='sens_q1')

        data_class = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q2')

        production = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q3')

        external_network = get_value_from_form_data(form_data=form_data,
                                                    form_prefix=form_prefix,
                                                    q_name=field.name,
                                                    value_name='sens_q4')

        if env_type_name != '' and data_class != '' and production != '' and \
            external_network != '' and form_data:
            option = get_sensitivity_type(q_1=env_type_name, \
                                          q_2=data_class, \
                                          q_3=production, \
                                          q_4=external_network)
            values = {'options': [(option, option)], 'initial_value': option}
            #LOGGER.info('{} values = {}'.format(field.name, values))
            return values

    elif field.name == 'sens_places':
        #Environment(s)
        env_type_name = get_value_from_form_data(form_data=form_data,
                                                 form_prefix=form_prefix,
                                                 q_name=field.name,
                                                 value_name='sens_q1')

        data_class = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q2')

        production = get_value_from_form_data(form_data=form_data,
                                              form_prefix=form_prefix,
                                              q_name=field.name,
                                              value_name='sens_q3')

        external_network = get_value_from_form_data(form_data=form_data,
                                                    form_prefix=form_prefix,
                                                    q_name=field.name,
                                                    value_name='sens_q4')

        if data_class != '' and env_type_name != '' and production != '' and \
            external_network != '' and form_data:
            sensitivity_type = get_sensitivity_type(q_1=env_type_name, \
                                                    q_2=data_class, \
                                                    q_3=production, \
                                                    q_4=external_network)

            order_group_id = form_data['order_group'][0]
            group = Group.objects.get(id=order_group_id)

            values = get_available_environments(sens_classification=sensitivity_type,
                                                group=group,
                                                include_unconstrained=True)

            #LOGGER.info('{} values = {}'.format(field.name, values))
            return values

    #if not all the questions have answers we can't decide what environment to
    #  build in yet, so we need a blank option
    values = create_blank_option()
    return values

def get_all_options_for_field(field=None):
    '''
        after submission of an order form, we need all the values possible
            for each c_f
    '''
    if field.name == 'sens_q1':
        values = get_sensitivity_options(q_1=True)
        #LOGGER.info('{} values = {}'.format(field.name, values))
    if field.name == 'sens_q2':
        values = get_sensitivity_options(q_2=True)
        #LOGGER.info('{} values = {}'.format(field.name, values))
    if field.name == 'sens_q3':
        values = get_sensitivity_options(q_3=True)
        #LOGGER.info('{} values = {}'.format(field.name, values))
    if field.name == 'sens_q4':
        values = get_sensitivity_options(q_4=True)
        #LOGGER.info('{} values = {}'.format(field.name, values))
    if field.name == 'sensitivity_classification':
        values = get_sensitivity_options(sens_type=True)
        #LOGGER.info('{} values = {}'.format(field.name, values))
    if field.name == 'sens_places':
        values = []
        for env in Environment.objects.all():
            values.append((env.name, env.name))
    return values

def create_blank_option():
    '''
        helper function to create a "nothing option"
    '''
    values = {'options': [('', '------')]}

    return values

def get_value_from_form_data(form_data=None, form_prefix=None,
                             q_name=None, value_name=None):
    '''
        helper function to get data from a dictionary (form_data)
        ***form list and order form values
    '''
    #LOGGER.info('get_value_from_form_data: {} -- {} -- {}: {}'.format(form_prefix,
    #                                                                  q_name,
    #                                                                  value_name,
    #                                                                  form_data))

    if form_data and form_prefix:
        if form_prefix + '-' + value_name in form_data:
            myval = form_data[form_prefix + '-' + value_name]
            if isinstance(myval, list):
                myvalue = myval[0]
            else:
                myvalue = myval
            LOGGER.info('get val: in {} --- {} = {}'.format(q_name,
                                                            value_name,
                                                            myvalue))
            return myvalue

    return ''

def get_available_environments(sens_classification=None,
                               group=None,
                               include_unconstrained=True):
    '''
        get the environments matching a sensitivity level from both the group
        permissions and any unconstrained environments
    '''

    #LOGGER.info('group id = {}'.format(group.id))
    #LOGGER.info('group name = {}'.format(group.name))
    matchingenvs = group.environments.filter(sensitivity_classification=sens_classification)

    options_dict = {} #dont allow dupes to get appended
    options = []
    if sens_classification == 'Restricted':
        options.append(('Manual Intervention required: email {}'.format(RESTRICTED_EMAIL_GROUP),
                        'Manual Intervention required: email {}'.format(RESTRICTED_EMAIL_GROUP)))

    for env in matchingenvs:
        #LOGGER.info('directly assigned group env name: {}'.format(env.name))
        if not env.name in options_dict:
            options_dict[env.name] = 1
            options.append((env.name, env.name))

    if include_unconstrained:
        for env in Environment.objects.filter(sensitivity_classification=sens_classification):
            if env.groups_served_by_inheritance.all().count() == 0:
                #unconstrained environment (all groups have access) -- filtered by classification
                if not env.name in options_dict:
                    options_dict[env.name] = 1
                    options.append((env.name, env.name))
                    #LOGGER.info('unconstrained env name: {}'.format(env.name))
    options_dict = {}
    #options count could be 0, 1 or many
    if not options:
        options.append(('No Environments Found for Sensitivity Classification',
                        'No Environments Found for Sensitivity Classification'))

    if len(options) == 1:
        #default to the first value if there is only one option
        values = {'options': options, 'initial_value': options[0][0]}
    else:
        values = {'options': options}

    return values

def get_sensitivity_options(q_1=None, q_2=None, q_3=None, q_4=None, sens_type=None):
    '''
        validation phase: get all the sensitivity classification options from the dict
    '''
    sensitivity_dict = get_sensitivity_dict()

    #sensitivity_dict[q1][q2][q3][q4] = 'Sensitivity Type' (DTN, Internal Sensitive, etc)
    no_dupes = {}
    options = []
    if q_1:
        for keyname in sensitivity_dict:
            #LOGGER.info('appending: {} -- {}'.format('q1', keyname))
            options.append((keyname, keyname))
    elif q_2:
        for myq1 in sensitivity_dict:
            for keyname in sensitivity_dict[myq1]:
                if not keyname in no_dupes:
                    #LOGGER.info('appending: {} -- {}'.format('q2', keyname))
                    options.append((keyname, keyname))
                    no_dupes[keyname] = 1
    elif q_3:
        for myq1 in sensitivity_dict:
            for myq2 in sensitivity_dict[myq1]:
                for keyname in sensitivity_dict[myq1][myq2]:
                    if not keyname in no_dupes:
                        #LOGGER.info('appending: {} -- {}'.format('q3', keyname))
                        options.append((keyname, keyname))
                        no_dupes[keyname] = 1
    elif q_4:
        for myq1 in sensitivity_dict:
            for myq2 in sensitivity_dict[myq1]:
                for myq3 in sensitivity_dict[myq1][myq2]:
                    for keyname in sensitivity_dict[myq1][myq2][myq3]:
                        if not keyname in no_dupes:
                            #LOGGER.info('appending: {} -- {}'.format('q4', keyname))
                            options.append((keyname, keyname))
                            no_dupes[keyname] = 1
    elif sens_type:
        for myq1 in sensitivity_dict:
            for myq2 in sensitivity_dict[myq1]:
                for myq3 in sensitivity_dict[myq1][myq2]:
                    for myq4 in sensitivity_dict[myq1][myq2][myq3]:
                        keyname = sensitivity_dict[myq1][myq2][myq3][myq4]
                        if not keyname in no_dupes:
                            LOGGER.info('appending: {} -- {}'.format('sens_type', keyname))
                            options.append((keyname, keyname))
                            no_dupes[keyname] = 1

    return options

def get_sensitivity_type(q_1=None, q_2=None, q_3=None, q_4=None):
    '''
        define the sensitivity classification from the questions answered
        in the order form.
    '''
    #sensitivity_dict[q1][q2][q3][q4] = 'Sensitivity Type' (DTN, Internal Sensitive, etc)
    sensitivity_dict = get_sensitivity_dict()

    if  q_1 in sensitivity_dict and \
        q_2 in sensitivity_dict[q_1] and \
        q_3 in sensitivity_dict[q_1][q_2] and \
        q_4 in sensitivity_dict[q_1][q_2][q_3]:
        myval = sensitivity_dict[q_1][q_2][q_3][q_4]

        #LOGGER.info('sensitivity_classification = {}'.format(myval))
        return myval

    options = []
    if  q_1 in sensitivity_dict and \
        q_2 is None:
        for keyname in sensitivity_dict[q_1]:
            #LOGGER.info('q2 appending {}'.format(keyname))
            options.append((keyname, keyname))
        return options

    if  q_1 in sensitivity_dict and \
        q_2 in sensitivity_dict[q_1] and \
        q_3 is None:
        for keyname in sensitivity_dict[q_1][q_2]:
            #LOGGER.info('q3 appending {}'.format(keyname))
            options.append((keyname, keyname))
        return options

    if  q_1 in sensitivity_dict and \
        q_2 in sensitivity_dict[q_1] and \
        q_3 in sensitivity_dict[q_1][q_2] and \
        q_4 is None:
        for keyname in sensitivity_dict[q_1][q_2][q_3]:
            #LOGGER.info('q4 - appending {}'.format(keyname))
            options.append((keyname, keyname))
        return options
    return 'Sensitivity Type not found!!!'

def get_sensitivity_dict():
    '''
        Get the options for the questions into a dict
    '''
    return  {
        'Cloud': {
            'Pub/Internal': {
                'Yes': {
                    'No': 'DTN'
                },
                'No': {
                    'No': 'Internal Sensitive'
                },
            },
            'Confidential': {
                'Yes': {
                    'No': 'Internal Sensitive'
                },
                'No': {
                    'No': 'DTS'
                },
            },
        },
        'Production': {
            'Pub/Internal': {
                'Yes': {
                    'Yes': 'External Sensitive',
                    'No': 'Internal Non-Sensitive',
                },
            },
            'Confidential': {
                'Yes': {
                    'Yes': 'External Sensitive',
                    'No': 'Internal Sensitive',
                },
            },
            'PCI': {
                'Yes': {
                    'Yes': 'Restricted',
                    'No': 'Restricted',
                }
            },
        },
        'Pre-Prod': {
            'Pub/Internal': {
                'No': {
                    'Yes': 'EDTS',
                    'No': 'DTN',
                }
            },
            'Confidential': {
                'No': {
                    'Yes': 'EDTS',
                    'No': 'DTS',
                }
            },
        },
    }